package com.sealfit.android.geoquakemobile.app;

import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.os.Bundle;

/**
 * Created by jgarcia on 8/10/14.
 */
public class GeoQuakeSplashFragment extends Fragment {


    public static GeoQuakeSplashFragment getInstance() {

        return new GeoQuakeSplashFragment();

    }


    private GeoQuakeSplashFragment(){
        // private constructor defined
        // use getInstance to return a new Fragment...
    }
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        this.setRetainInstance(true);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View mSplashScreenFragment = inflater.inflate(R.layout.fragment_splash_screen, container, false);
        return mSplashScreenFragment;

    }





}
