package com.sealfit.android.geoquakemobile.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.view.View;
import android.app.Activity;
import android.content.Intent;

/**
 * Created by jgarcia on 3/4/15.
 */
public class OstesNotificationActivity extends Activity {

    public void createNotification(){

        Intent intent = new Intent(this.getApplicationContext(), GeoQuakeMapFragment.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new Notification.Builder(this).setContentTitle("A large earthquake has occured.")
                                                                  .setContentText("A large Seismic event of 5.9 magnitude has occured")
                                                                  .setContentIntent(pIntent)
                                                                  .addAction(R.drawable.powered_by_google_light, "Open", pIntent).build();



        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

       notification.flags |= Notification.FLAG_AUTO_CANCEL;
       notificationManager.notify(0, notification);
    }


}
