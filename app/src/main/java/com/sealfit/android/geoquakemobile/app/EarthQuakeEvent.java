package com.sealfit.android.geoquakemobile.app;

import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONObject;

import android.location.Location;
import android.util.Log;



public class EarthQuakeEvent  {
    public static final String EARTHQUAKE = "EarthQuakeEvent";



    private  UUID      mUuid;       // internal id that we can use for tracking particular events.
    private  UUID      mEventId;
    private  String    mEventName;
    private  Location  mEventLocation;        // Google Map Location object.
    private  double    mLatitude;
    private  double    mLongitude;
    private  float     mEventMagnitude;
    private  int       mSig;  // significance of an event [0,1000] is the range.
    private  long      mEventTime;
    private  String    mEventAlertColor;
    private  float     mEventDepth;
    private  String    mEventType;
    private  JSONArray mJsonArray;
    private  JSONObject mJsonObject;
    private  String    mDetail;
    private static long mEventCount = 0;


    // newInstance method.  We use this technique because we are not
    // planning on sub-classing this class.
    public static EarthQuakeEvent newInstance(JSONObject json_object ){

        return new EarthQuakeEvent(json_object);
    }

    // private constructor
    private EarthQuakeEvent(JSONObject json_object){




        // Json array that we get from the USGS.  this object
        // is fed from the EarthQuakeData object.
        mJsonObject = json_object;


        try {

            this.setEventName(mJsonObject.getJSONObject("properties").getString("place"));
            this.setType( mJsonObject.getJSONObject("properties").getString("type") );
            this.setMagnitude( (float) mJsonObject.getJSONObject("properties").getDouble("mag") );
            this.setSig((int) mJsonObject.getJSONObject("properties").getDouble("sig")); // Significance of event.
            this.setDetail( mJsonObject.getJSONObject("properties").getString("url") );

            this.setAlert( mJsonObject.getJSONObject("properties").getString("alert") );
            this.mLatitude = mJsonObject.getJSONObject("geometry").getJSONArray("coordinates").getInt(1);
            this.mLongitude = mJsonObject.getJSONObject("geometry").getJSONArray("coordinates").getInt(0);
            this.setEventTime(mJsonObject.getJSONObject("properties").getLong("time"));    // Used to trend on time...


        } catch (Exception e){
            Log.e(EARTHQUAKE, "Error in JSON string, cannot get type");
            Log.e(EARTHQUAKE, "Error in JSON String: " + e.getCause());
        }


        this.setEventId( UUID.randomUUID());  // generate a unique UUID



    }


    // Typical Setters

    // This event id is the id that comes from the
    // GeoJson object from the USGS.
    // We do not use this id internally
    // for any


    public void setEventId(UUID id){

        if(id != null){
            mEventId = id;
            Log.d(EARTHQUAKE, "Random id for this event: " + id);
        } else {
            Log.e(EARTHQUAKE, "null id passed in from datasource. Earthquake event will not have an ID.");

        }

    }
    public  void setEventName(String name){

        if(name != null){
            mEventName = name;
        } else {
            Log.e(EARTHQUAKE, "Null passed in for event name, event will not have a name");

        }

    }
    public void  setLocation(float lat, float lon ){

        mEventLocation.setLatitude(lat);
        mEventLocation.setLongitude(lon);


    }
    public void setMagnitude( float magnitude){

        mEventMagnitude = magnitude;


    }
    public void setSig(int sig){

        mSig = sig;

    }
    public void setEventTime(long time){

        mEventTime = time;



    }
    public void setDetail(String detail){

        this.mDetail = detail;
    }
    public void setAlert(String alertColor){

        mEventAlertColor = alertColor;


    }
    public void setDepth(float depth){

        mEventDepth = depth;



    }

    public void setType(String type){

        mEventType = type;



    }

    public static long getEventCount(){
        return mEventCount;
    }





    // Typical Getters

    public UUID getEventId() {

        return mEventId;
    }

    public String getEventName() {

        return mEventName;
    }

    public Location getLocation( ) {

        return mEventLocation;
    }

    public String getDetail(){

        return mDetail;
    }

    public float getMagnitude() {

        return mEventMagnitude;
    }

    // Used to determine the statistical significance of a
    // particular seismic event.
    public float getSig(){

        return mSig;
    }

    public long getEventTime() {

        return mEventTime;
    }

    public String getAlert() {

        return mEventAlertColor;
    }

    public float getDepth() {
        return mEventDepth;
    };

    public String getType(){

        return mEventType;

    }
    public double getLatitude(){
        return mLatitude;
    }
    public double getLongitude(){
        return mLongitude;
    }





}
