package com.sealfit.android.geoquakemobile.app;

/**
 * Created by jgarcia on 3/4/15.
 */

//import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.content.Context;
import android.view.ViewGroup;
import com.esri.android.map.MapView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.SupportMapFragment;





public class OstesMapFragment extends MapFragment {
   private GoogleMap mGoogleMap;
   Context mContext;

    public static OstesMapFragment newMapInstance() {

        return new OstesMapFragment();

    }
    @Override
    public void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
       setRetainInstance(true);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View MapView = inflater.inflate(R.layout.fragment_map_view,container, false);

       this.initializeMap(MapView);



        return MapView;



    }

    private void initializeMap( View mapView){

        if(mapView == null){

            mGoogleMap = ((MapFragment)getFragmentManager().findFragmentById(R.id.map) ).getMap();
            mGoogleMap.setMyLocationEnabled(true);
        }

    }



}
