package com.sealfit.android.geoquakemobile.app;

// This is used to display the seismic events on the device.

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
/**
 * Created by jgarcia on 8/10/14.
 */
import com.google.android.gms.*;


public class GeoQuakeEventList extends ListFragment  {

   private static final String MAP_TAG = "map_tag";


    public static GeoQuakeEventList getInstance(){

        return new GeoQuakeEventList();

    }

    private GeoQuakeEventList(){
      // Do not allow us to call the constructor....

    }

    // basic construction and initialization.....
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);




    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View EventListFragment = inflater.inflate(R.layout.fragment_item_list  , container, false);

       return EventListFragment;
    }



}
