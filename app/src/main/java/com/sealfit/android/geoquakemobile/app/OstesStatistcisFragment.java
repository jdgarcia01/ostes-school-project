package com.sealfit.android.geoquakemobile.app;



import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.widget.EditText;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by jgarcia on 6/8/15.
 */
public class OstesStatistcisFragment extends Fragment {

    private static final String OstesStatistic = "OstesStatisticsFragment";
    // attributes of this fragment.
    private SharedPreferences mApplicationPrefs;
    private GetStatisticData mJsonData;
    private SharedPreferences mListViewPrefs;
    private int mUrlResourceId;
    private String mUrlName;
    private EarthQuakeEvent mEarthQuakeEventInfo;
  //  private WeakReference<GetGeoJsonData>asyncTaskWeakRef;
    protected EditText mAverageEditText;


    public static OstesStatistcisFragment newInstance(){

        return (new OstesStatistcisFragment());


    }

    @Override
    public void onCreate(Bundle savedInstance){

        // Call the parent/super onCreate per Android Docs.
        super.onCreate(savedInstance);

        this.setRetainInstance(true);


        // load a toast message, notifying the user what
        // data setting they are currently using.
        mListViewPrefs = getActivity().getSharedPreferences("com.example.sealfit.android.geoquakemobile.prefs", Context.MODE_PRIVATE);


        mUrlResourceId = mListViewPrefs.getInt("radio_string", 0);
        mUrlName = getActivity().getResources().getString(mUrlResourceId);

        if(mUrlName != null){

            Toast.makeText(getActivity(), "Showing all events past " + mUrlName, Toast.LENGTH_SHORT).show();

        }

        Log.d(OstesStatistic, "the selected url is: " + mUrlName);

       mJsonData = new GetStatisticData();

        mJsonData.execute(mUrlName);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup view, Bundle savedInstanceState){

        View statisticsView = inflater.inflate(R.layout.fragment_statistics, view, false);

        mAverageEditText = (EditText)statisticsView.findViewById(R.id.editTextAveMagnitude);






        return statisticsView;

    }




    // TODO- remove all async inner classes and use only one data source to
    // TODO  populate fragments that display data.




    private class GetStatisticData extends AsyncTask<String,     Integer,       Float > {

        // String used for logging purposes...
        private static final String EarthQuakeDataHttpConnection = "AsyncTaskStatisticsFragment";
      //  private WeakReference<OstesListView> fragmentWeakRef;
      private float mAverageMag = 0.0f;


        @Override
        protected Float doInBackground(String... url){

            Log.d(EarthQuakeDataHttpConnection, "URL IS: " + url[0].toString());

            ArrayList<EarthQuakeEvent> earthQuakeData;
            Log.d(EarthQuakeDataHttpConnection, "URL IS: " + url[0].toString());
            mAverageMag =  this.getGeoJsonData(url[0].toString());
            return mAverageMag;

        }



        // TODO -- modify this method to accept the Vector/HashMap of the EarthQuakes return from the
        // TODO -- doInBackground method.
        // TODO -- Don't worry.  We need not do the above anymore.
        @Override
        protected void onPostExecute(Float result){

            // Update the list here....
            Log.i(EarthQuakeDataHttpConnection, "result is: " + result);

            // Update the edit view with the average magnitude.
            mAverageEditText.setText(result.toString());




        }



        /*
         *
         * @param url of the datasource of the JSON object.
         *
         * @return A json object from the datasource.
         */
        public float getGeoJsonData(String url){


            HttpClient mHttpClient = new DefaultHttpClient();
            HttpGet mHttpGet = new HttpGet(url);
            String result = null;
            JSONObject mGeoJsonObject = null;

            HttpResponse response;
            float average_mag = 0.0f;
            float ave = 0.0f;

            try {
                response = mHttpClient.execute(mHttpGet);
                HttpEntity entity = response.getEntity();

                if(entity != null){

                    InputStream mInstream = entity.getContent();
                    result = this.convertStreamToString(mInstream);

                    // We have a JSONObject from the datasource.  We need to create the SeismicEvents
                    // based on the "type" of event.
                    mGeoJsonObject = new JSONObject(result);



                    // Instantiate an EarthQuakeEvent object....add it to the ArrayList.
                    // We then put the
                    for(int c = 0; c < mGeoJsonObject.getJSONArray("features").length(); c++) {
                        /******* This is here for development/debugging purposes.  Do not put into production *****/
                        Log.d(EarthQuakeDataHttpConnection, "Features from datafeed: " + mGeoJsonObject.getJSONArray("features").getString(c));
                        Log.d(EarthQuakeDataHttpConnection, "Features type: " +          mGeoJsonObject.getJSONArray("features").getJSONObject(c).getJSONObject("properties").getString("type"));
                        Log.d(EarthQuakeDataHttpConnection, "Features type: " +          mGeoJsonObject.getJSONArray("features").getJSONObject(c).getJSONObject("properties").getString("mag"));
                        average_mag = average_mag + Float.valueOf(mGeoJsonObject.getJSONArray("features").getJSONObject(c).getJSONObject("properties").getString("mag"));

                        Log.d(EarthQuakeDataHttpConnection, "Feature Longitude: " +      mGeoJsonObject.getJSONArray("features").getJSONObject(c).getJSONObject("geometry").getJSONArray("coordinates").get(0));
                        Log.d(EarthQuakeDataHttpConnection, "Feature Latitude: " +       mGeoJsonObject.getJSONArray("features").getJSONObject(c).getJSONObject("geometry").getJSONArray("coordinates").get(1));
                        // Build the Vector/HashMap of EarthQuake Objects...
                        // We then return this Vector/HashMap as a result and feed this to the onPostExecute method.

                        // TODO -- Fix the bug when we parse the data.  We are passing in the features and the same
                        //        Data keeps reappearing.  We are passing in the wrong data.
                        // Fixed -- on 08/23/2014.
                   //     mEarthQuakeList.add(EarthQuakeEvent.newInstance(mGeoJsonObject.getJSONArray("features").getJSONObject(c)));



                    }

                 Log.d(EarthQuakeDataHttpConnection, "The sum of magnitudes : " + average_mag);
                 Log.d(EarthQuakeDataHttpConnection, "The number of events is: " + mGeoJsonObject.getJSONArray("features").length());
                 Log.d(EarthQuakeDataHttpConnection, "Average magnitude is: " + ( average_mag / mGeoJsonObject.getJSONArray("features").length() ));
                 ave = ( average_mag / mGeoJsonObject.getJSONArray("features").length() );

                }



            } catch (Exception e) {
                Log.e(EarthQuakeDataHttpConnection, "Error with http connection-> "+ e.getMessage() );
            }

            // Return the ArrayList...this goes to the postExecute method.
            return ave;

        }


        /***
         * @param input stream, this comes in the form of a JSON input stream that needs to be converted to
         *              String Object.
         *  @return Returns a String object.  We can do what we need with this object.
         *
         */
        private String convertStreamToString(InputStream is) {
				    /*
				     * To convert the InputStream to String we use the BufferedReader.readLine()
				     * method. We iterate until the BufferedReader returns null which means
				     * there's no more data to read. Each line will be appended to a StringBuilder
				     * and returned as String.
				     */
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();

            String line = null;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(EarthQuakeDataHttpConnection, "Error while reading or building string: " + e.getMessage());
            } finally {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return sb.toString();
        }

    }





}
