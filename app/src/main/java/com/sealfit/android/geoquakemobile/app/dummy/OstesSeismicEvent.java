package com.sealfit.android.geoquakemobile.app.dummy;

import java.util.Date;

/**
 * Created by alexander_the_great on 11/27/15.
 */
public class OstesSeismicEvent  {

    private double mMagnitude;
    private double mDepth;
    private Date mEventDate;
    private String Location;



    public OstesSeismicEvent(double mag, double depth, Date event_date, String loc){

        mMagnitude = mag;
        mDepth     = depth;
        mEventDate = event_date;
        Location   = loc;

    }

    public double getmDepth() {
        return mDepth;
    }

    public Date getmEventDate() {
        return mEventDate;
    }

    public String getLocation() {
        return Location;
    }

    public double getmMagnitude() {

        return mMagnitude;
    }
}
